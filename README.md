## Introduction

I just wanted to start a simple version of the Tell Dont Ask issue with the example of Cpu Monitors



### 1st round
I just start as a simple TDD exercise. What is asked it that a class (called AlertCentral for the moment) should use one monitor and verify that is that monitor is other the threshold then it shoud return true.

No refactoring at this point, to let the discussion open.

It's very naive and non production code of course.

### 2nd round
--- thinking ---

Now, we go with more real life example.
In real life, it is intersting to watch over multiples monitors.
Then the number of test cases is increasing, and obviously the type Monitor should be created outise the main Component (AlertCentral is still its name).
Then it's easier to create different test cases whithout changing the SUT (System Under Test). 
Having multiple test cases should not change the SUT. They are only changing "parameters".
The question is "how can I let my SUT know the different cases". Let's start with a basic approach.

--- coding ---

that way of coding implies one new major change: we have to let the Universe know that we use a HeatMonitor class; and it can be used freely.
Is it a good design? Too early to call.

Also, to avoid to code too fast (and that is one of the most important principle in TDD), do not add a test until the refactor is ok with the existing test
So for now, I commit only the design that a list of Monitors should be passed to the SUT, and the list contain only one item for now.

### 3rd round
--- thinking ---

okayyy, let's try with a 2nd monitor.
Of course, I cheated a little bit.
As the interest of TDD is to write failing test, as I do know the fallacy in my previous implementation, I decide to arrange the facts in a way that I'm sure that I will be a perfect case for failure.
And I think it's important to write it that way: how gonna make it fail? WHat are the genuine examples that my code doesn't handle yet?

--- writing a second test; failed ---

I also try to use the debugger in VS Code, for that I didn't launch the tests from the ".Net test explorer" left window; but I go to the code of the test
and hover the name of the [Fact] function, I right click on 'Debug Test in context' 
Stackoverflow helped me (sic), though I should have remembered that :/
https://stackoverflow.com/questions/60593663/vscode-dotnet-test-explorer-debugging-test-waiting-for-process-to-attach


--- coding ---
Naturaly, as I've choose to use a list (developers love lists), I write a loop to check every element in the list.
at this point, it is intentional (for this exercise) to add a bug in my implementation.

--- thinking ---

What happened if the program ran out of the list? 
Well... nobody told me.
As a developer, what am I supposed to do?
I have a terrible biais, that is the language I use, the compiler, and my bad habbits.

My code is already weak. Because I use a poor typing. In fact I realize that I use the worst type of data in the universe: booleans!

Booleans mean nothing. In a business conversation, do you ever answer 'true' or 'false' to a question like 'what is the temperature of this room?'


### 4th round
--- thinking ---
So little lines of code, so many questions!
Well; boolean tells nothing. It doesn't express a thing.
What true really means? It depends on the question.
IsTheSystemOverHeating: true!
IsTheSystemOkay:  true!  
If you invert the question, the answer have not the same value. So you never never know what a booleans means.
When it comes to expose business, you should NEVER use booleans.
You will find an infinity of articles about that topic on the internets  
https://dev.to/macsikora/boolean-the-good-the-bad-and-there-is-no-place-for-the-ugly-2o0h
https://understandlegacycode.com/blog/what-is-wrong-with-boolean-parameters/

You think I'm going too far?
Absolutley not. Because here is a typical case of business.
The AlertCentral class is expressing business. A public business need.
That means that there is a conversation between this class and the outside world.
I'm doing a functional testing, I want to test a behaviour, not the implementation.
It should be black box testing https://tinyurl.com/y6rnwk5n
Well, it's not perfect now, because I have this HeatMonitor class showing off, and it can be considered as an inside element that I don't need to know in a black box testing. We'll talk about that later.

What is most important is how I can understand what's happening, only by reading the test, not opening the source.
And booleans cannot help me.
I need to be more explicit.
Even if it's to say 'yes' instead of 'true'. Yes is an real answer.
And 'no answer' can be an answer ;)


--- coding ---

In my case, I first do the refactor with an enum saying  OverHeating or Normal; as the result of the system's global state.
The test are more readable: now it is clear that I'm asking for the Global State of the system; I rename the property to reflect that and to ensure that
the type of the result makes sense with the name of the property that hold this value.


--- thinking ---

A property is well named when that name make sense with the name of its type. It's a duet.
One cannot live without the sense of the other. The property (or argument) name has to be coherent with its type.
That is something that comes particulary clear in mind when you write the tests in a BDD manner.
And I'm going to go quickly into that.

Another thing: now that I have more expressive types and properties, it also come really clear that my tests are not enough.
Some cases are not written.
For instance; I still haven't handle the case when my system is normal.
Of course, I should use a coverage test tool to see that my line 'return AlertCentralStatus.Normal;' isn't reached yet.
Or I could use mutation testing, and it will quickly appears that why I change this line of code, not one test is failing :(
    I have to try this one:  https://stryker-mutator.io/docs/mutation-testing-elements/mutant-states-and-metrics/

And again a lot of questions are coming!
Am I going to add more input cases randomly and browse all possible combinations of inputs?
Or am I looking to prove that it exists at least one case that leads to the result  AlertCentralStatus.Normal.

At this point, I'm open to both approaches.








