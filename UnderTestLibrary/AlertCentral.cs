using System.Collections.Generic;
using System.Linq;

namespace UnderTestLibrary
{

    public enum AlertCentralStatus
    {
        Normal,
        OverHeat
        
    }

    public class AlertCentral
    {
        List<HeatMonitor> listOfMonitors;
        public AlertCentral(List<HeatMonitor> listOfMonitors)
        {
            this.listOfMonitors = listOfMonitors;
        }

        public AlertCentralStatus  GlobalStatus
        {
            get
            {
              foreach (var heatMonitor in listOfMonitors)
                {    if (heatMonitor.Temperature >= 90)
                    return AlertCentralStatus.OverHeat;
                }
                return AlertCentralStatus.Normal;               
            }
        }
    }
}